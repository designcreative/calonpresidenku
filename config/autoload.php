<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$autoload['packages'] = array();
$autoload['libraries'] = array("database","session");	//ditambahin
$autoload['helper'] = array("url","html","form");	//ditambahin
$autoload['config'] = array('application');
$autoload['language'] = array();
$autoload['model'] = array('model_global');
/* Location: ./application/config/autoload.php */