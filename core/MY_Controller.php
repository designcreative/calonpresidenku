<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

  public $template = array();
  public $is_auth = 0;
  public $userinfo = array();

  public $response;

	function __construct() {
		$signed_request = isset($_REQUEST['signed_request'])?$_REQUEST['signed_request']:'';
		$code = isset($_REQUEST['code'])?$_REQUEST['code']:'';
		
    	parent::__construct();

		$this->output->set_header('p3p: CP="NOI ADM DEV PSAi COM NAV OUR OTR STP IND DEM"');
		
    	if (!empty($signed_request))
			$_REQUEST['signed_request'] = $signed_request;
		if (!empty($code))
			$_REQUEST['code'] = $code;

	    $this->response = $this->session->flashdata('response');

		$this->template['title'] = 'Design Creative | Support';
  	}

  	function cek_null($val=''){
		return ($val=='') ? NULL : $val;
	}

	function render_page($page){
		$this->load->view('header', $this->template);
		$this->load->view($page, $this->template);
		$this->load->view('footer', $this->template);
	}

	/* Get Data from API */
	public function promises($type = '')
	{
        /* get data promises per calon */
		$jokowi = $this->get_promises_calon('jw');
        $prabowo = $this->get_promises_calon('ps');

        /* get random data promises per calon dan per 10 item */
        $rand_jokowi = array_rand($jokowi['data']['results']['promises'], 10);
        $rand_prabowo = array_rand($prabowo['data']['results']['promises'], 10);
        
        /* store data to new array for comparison question */
        $data = array();
        for ($i=0; $i < 10; $i++) { 
            $data['result'][0][] = array('id' => $jokowi['data']['results']['promises'][$rand_jokowi[$i]]['id'], 'question' => $jokowi['data']['results']['promises'][$rand_jokowi[$i]]['context_janji']);

            $data['result'][1][] = array('id' => $prabowo['data']['results']['promises'][$rand_prabowo[$i]]['id'], 'question' => $prabowo['data']['results']['promises'][$rand_prabowo[$i]]['context_janji']);
        }

        if($type == 'json'){
            header("Content-type: application/json");
            echo json_encode($data);
        }
        else{
            return $data;
        }
	}

    public function get_promises_by_id($id){
        $url = "http://api.pemiluapi.org/calonpresiden/api/promises/".$id."?apiKey=fea6f7d9ec0b31e256a673114792cb17";
        
        //open connection
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_URL, $url);

        //execute post
        $result = json_decode(curl_exec($ch), true);

        //close connection
        curl_close($ch);

        return $result;
    }

    public function get_promises_calon($calon){
        $url = "http://api.pemiluapi.org/calonpresiden/api/promises?apiKey=fea6f7d9ec0b31e256a673114792cb17&id_calon=".$calon;

        //open connection
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_URL, $url);

        //execute post
        $result = json_decode(curl_exec($ch), true);

        //close connection
        curl_close($ch);

        return $result;
    }

    public function get_calon_presiden($type = '', $id_calon = ''){
        $url = '';
        if($id_calon != ''){
            $url = "http://api.pemiluapi.org/calonpresiden/api/caleg/".$id_calon."?apiKey=fea6f7d9ec0b31e256a673114792cb17";
        }
        else{
            $url = "http://api.pemiluapi.org/calonpresiden/api/caleg?apiKey=fea6f7d9ec0b31e256a673114792cb17";  
        }

        //open connection
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_URL, $url);

        //execute post
        $result = json_decode(curl_exec($ch), true);

        //close connection
        curl_close($ch);

        if($type == 'json'){
            header("Content-type: application/json");
            echo json_encode($result);
        }
        else{
            return $result;
        }
    }
    /* END GET DATA API */
}
