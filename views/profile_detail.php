<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<style>
	.candidate-image { text-align: center; }
	.candidate-image .img-candidate { width: 100px; border-radius: 50px; }
	.candidate-bio { background: #d91919; color: #fff; margin-top: 5px; }
	.candidate-bio:hover { cursor: pointer; }
	.candidate-bio h5 { padding: 0; margin: 0 }
	.candidate-bio h5:hover { cursor: pointer; }

	.modal-title, .modal-body { color: #2c2c2c; }
</style>
<div class="container">
	<div id="profile-detail" class="row">
		<!-- START LOOP FROM HERE -->
		<?php
			$count = count($data); 
			$final_count = 0;
			if($count == 1){
				$final_count = 12;
			}else if($count == 2){
				$final_count = 6;
			}else if($count == 3){
				$final_count = 4;
			}else if($count == 4){
				$final_count = 3;
			}
		?>
		<?php foreach($data as $row): ?>
			<div class="col-md-<?=$final_count?> pad-top-10">
				<div class="pad-10 border">
					<div class="candidate-image">
						<span><img src="<?=base_url()?>assets/img/<?=$row['id']?>.png" class="img-candidate" /></span>
						<h4><?=$row['nama']?></h4>
					</div>
				</div>
				<div class="candidate-bio pad-10 border" data-target="#bio-<?=$row['id']?>">
					<h5>Biography <span class="pull-right"><i class="fa fa-chevron-right"> </i></span></h5>
				</div>
				<!-- Modal -->
				<div class="modal fade" id="bio-<?=$row['id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="myModalLabel">
									<!-- TITLE HERE -->
									Biography - <span class="font-red"><?=$row['nama']?></span>
								</h4>
							</div>
							<div class="modal-body" style="text-align: justify">
								<!-- CONTENT HERE -->
								<?=$row['biografi']?>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- End Modal -->
				<div class="candidate-bio pad-10 border" data-target="#pendidikan-<?=$row['id']?>">
					<h5 >Riwayat Pendidikan <span class="pull-right"><i class="fa fa-chevron-right"> </i></span></h5>
				</div>
				<!-- Modal -->
				<div class="modal fade" id="pendidikan-<?=$row['id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="myModalLabel">
									<!-- TITLE HERE -->
									Riwayat Pendidikan - <span class="font-red"><?=$row['nama']?></span>
								</h4>
							</div>
							<div class="modal-body" style="text-align: justify">
								<!-- CONTENT HERE -->
								<ul>
								<?php foreach($row['riwayat_pendidikan'] as $row2): ?>
									<li>
										<span><b><?=$row2['ringkasan']?></b></span>
										<small><?=$row2['tanggal_mulai']?> - <?=$row2['tanggal_selesai']?></small>
									</li>
								<?php endforeach; ?>
								</ul>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- End Modal -->
				<div class="candidate-bio pad-10 border" data-target="#pekerjaan-<?=$row['id']?>">
					<h5 >Riwayat Pekerjaan <span class="pull-right"><i class="fa fa-chevron-right"> </i></span></h5>
				</div>
				<!-- Modal -->
				<div class="modal fade" id="pekerjaan-<?=$row['id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="myModalLabel">
									<!-- TITLE HERE -->
									Riwayat Pekerjaan - <span class="font-red"><?=$row['nama']?></span>
								</h4>
							</div>
							<div class="modal-body" style="text-align: justify">
								<!-- CONTENT HERE -->
								<ul>
								<?php foreach($row['riwayat_pekerjaan'] as $row2): ?>
									<li>
										<span><b><?=$row2['ringkasan']?></b></span>
										<small><?=$row2['tanggal_mulai']?> - <?=$row2['tanggal_selesai']?></small>
									</li>
								<?php endforeach; ?>
								</ul>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- End Modal -->
				<div class="candidate-bio pad-10 border" data-target="#organisasi-<?=$row['id']?>">
					<h5>Riwayat Organisasi <span class="pull-right"><i class="fa fa-chevron-right"> </i></span></h5>
				</div>
				<!-- Modal -->
				<div class="modal fade" id="organisasi-<?=$row['id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="myModalLabel">
									<!-- TITLE HERE -->
									Riwayat Organisasi - <span class="font-red"><?=$row['nama']?></span>
								</h4>
							</div>
							<div class="modal-body" style="text-align: justify">
								<!-- CONTENT HERE -->
								<ul>
								<?php foreach($row['riwayat_organisasi'] as $row2): ?>
									<li>
										<span><b><?=$row2['ringkasan']?></b></span>
										<span><b><?=$row2['jabatan']?></b></span>
										<small><?=$row2['tanggal_mulai']?> - <?=$row2['tanggal_selesai']?></small>
									</li>
								<?php endforeach; ?>
								</ul>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- End Modal -->
				<div class="candidate-bio pad-10 border" data-target="#penghargaan-<?=$row['id']?>">
					<h5>Riwayat Penghargaan <span class="pull-right"><i class="fa fa-chevron-right"> </i></span></h5>
				</div>
				<!-- Modal -->
				<div class="modal fade" id="penghargaan-<?=$row['id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="myModalLabel">
									<!-- TITLE HERE -->
									Riwayat Penghargaan - <?=$row['nama']?>
								</h4>
							</div>
							<div class="modal-body" style="text-align: justify">
								<!-- CONTENT HERE -->
								<ul>
								<?php foreach($row['riwayat_penghargaan'] as $row2): ?>
									<li>
										<span><b><?=$row2['ringkasan']?></b></span>
										<span><b><?=$row2['institusi']?></b></span>
										<small><?=$row2['tanggal']?></small>
									</li>
								<?php endforeach; ?>
								</ul>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- End Modal -->
			</div>
		<?php endforeach; ?>
		<!-- END LOOP FROM HERE -->
	</div>
	<div style="text-align: center; margin-top: 20px;">
		<a href="<?=base_url()?>wiki" class="btn btn-danger" style="font-size: 18px">Lihat lainnya!</a>
	</div>
</div>

<script>
$(document).ready(function() {
	var ua = navigator.userAgent;
    var events = (ua.match(/iPad/i)) || (ua.match(/iPhone/i)) ? "touchstart" : "click";

    $('div.candidate-bio').on(events, function(){
    	var target = $(this).attr('data-target');
    	$(target).modal('toggle');
    });
});
</script>