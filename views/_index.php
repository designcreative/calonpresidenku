<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Questioneer</title>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.validate.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			/* Ajax to get data */
			// $.ajax({
			// 	url: "<?=base_url()?>data/promises/json",
			// 	dataType: "JSON"
			// }).done(function(result) {
			// 	console.log(result);
			// });
			/* submit form */
			jQuery("#myform").validate();
			jQuery("#myform").submit(function() {
		    	if(jQuery("#myform").valid()){
			        jQuery.ajax({
			            type: "POST",
			            url: "<?=base_url()?>question/submit",
			            data: jQuery("#myform").serialize(),
			            dataType: "JSON"
			        }).done(
			            function (result) {
			            	console.log(result);
			            	if(result['data']['results']['count'] > 0){
				            	$('#content').hide();
				            	$('#success').show();
				            }
			        	}
			        );
			    }
			    return false;
			});
		});
	</script>
</head>
<body>
	<div id="content">
		<form id="myform" action="javascript:void(0)" method="post">
		<?php for($i = 1; $i <= 10; $i++): ?>
		<?php $rand = rand(0,1); ?>
			<div id="page-<?=$i?>">
				<h3>Pilih mana?</h3>
				<?php if($rand == 0): ?>
					<input type="radio" class="required" name="q-<?=$i?>" value="<?=$data['result'][0][$i-1]['id']?>" /> <?=$data['result'][0][$i-1]['question']?>
					<input type="radio" class="required" name="q-<?=$i?>" value="<?=$data['result'][1][$i-1]['id']?>" /> <?=$data['result'][1][$i-1]['question']?>
				<?php else: ?>
					<input type="radio" class="required" name="q-<?=$i?>" value="<?=$data['result'][1][$i-1]['id']?>" /> <?=$data['result'][1][$i-1]['question']?>
					<input type="radio" class="required" name="q-<?=$i?>" value="<?=$data['result'][0][$i-1]['id']?>" /> <?=$data['result'][0][$i-1]['question']?>
				<?php endif; ?>
			</div>
		<?php endfor; ?>
		<br>
		<input type="submit" value="Submit">
		</form>
	</div>
	<div id="success" style="display:none">
		Success
	</div>
</body>
</html>