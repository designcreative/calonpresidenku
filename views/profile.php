<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

<div id="profile">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div id="logo">
					<h2>Tarik dan masukkan ke dalam box untuk melihat informasi tentang</h2>
					<h1 style="padding: 0"><span class="font-red">Calon Presidenku & Wakil Presidenku</span></h1>
				</div>
			</div>
		</div>
	</div>
	<br/>
	<div id="content">
		<div class="container" style="width: 400px">
			<div id="default-drop-point" class="row">
				<div class="col-xs-3" data-id="ps">
					<div id="candidate" class="candidate">
						<span><img src="<?=base_url()?>assets/img/ps.png" class="img-candidate-wiki" /></span>
						<h4>Prabowo Subianto</h4>
					</div>
				</div>
				<div class="col-xs-3" data-id="hr">
					<div id="candidate" class="candidate">
						<span><img src="<?=base_url()?>assets/img/hr.png" class="img-candidate-wiki" /></span>
						<h4>Hatta Rajasa</h4>
					</div>
				</div>
				<div class="col-xs-3" data-id="jw">
					<div id="candidate" class="candidate">
						<span><img src="<?=base_url()?>assets/img/jw.png" class="img-candidate-wiki" /></span>
						<h4>Joko Widodo</h4>
					</div>
				</div>
				<div class="col-xs-3" data-id="jk">
					<div id="candidate" class="candidate">
						<span><img src="<?=base_url()?>assets/img/jk.png" class="img-candidate-wiki" /></span>
						<h4>Jusuf Kalla</h4>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container" style="width: 400px">
		<div class="row">
			<div class="col-xs-12">
				<div id="drop-point" class="row"></div>	
			</div>
		</div>
	</div>

	<div style="text-align: center; margin-top: 20px;">
		<button class="btn btn-danger" style="font-size: 18px" onclick="showDetail();">Lihat Hasil</button>
	</div>
</div>

<div id="profile-detail">
</div>

<script src="http://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.2/jquery.ui.touch-punch.min.js"></script>
<script>
var count = 0;
$(function(){
	$('.col-xs-3').draggable({ revert: true });
	$( "#drop-point" ).droppable({
		drop: function(e, ui){
			var html = '';
			var $x = ui.draggable;
			
			var id = ui.draggable.attr('data-id');

			length = $x.find("input").length;
			
			if(length == 0)
			{
				var html = '<input type="hidden" class="calon" name="calon-'+count+'" value="'+id+'" />';
			}

			$x.append(html);

	        $x.appendTo($("#drop-point")).css({
	            top:0,
	            left:0
	        });
	        count++;
		}
	});
	$( "#default-drop-point" ).droppable({
		drop: function(e, ui){
			var $x = ui.draggable;
			
			$x.find("input").remove();

	        $x.appendTo($("#default-drop-point")).css({
	            top:0,
	            left:0
	        });

	        count--;
		}
	});
});

function showDetail(){
	var url = '';
	$('.calon').each(function(){
		if(url == ''){
			url = this.value;
		}
		else{
			url += ',' + this.value;
		}
	});

	window.location = base_url + 'wiki/detail?calon=' + url; 
}
</script>