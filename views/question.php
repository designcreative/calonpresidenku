<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

<script type="text/javascript">
	function nextPage(id){
		$('#question-container').animate({ "margin-left": "-=300" });
	}
</script>
<div id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div id="logo" class="question">
					<h1 class="font-red">Calon Presidenku<br/>& Wakil Presidenku</h1>
					<img src="<?=base_url()?>assets/img/ps.png" class="img-candidate" />
					<img src="<?=base_url()?>assets/img/hr.png" class="img-candidate" />
					<img src="<?=base_url()?>assets/img/jw.png" class="img-candidate" />
					<img src="<?=base_url()?>assets/img/jk.png" class="img-candidate" />
				</div>
				<div id="question-title">
					Menurut kamu mana yang lebih baik ?
				</div>
				<div id="question-answer">
					<form id="myform" action="javascript:void(0)" method="post">
						<div id="question-container">
							<?php $i = 1; $max_length = 10; ?>
							<?php for($i=1;$i<=$max_length;$i++){ ?>
							<?php $rand = rand(0,1); ?>
								<div id="page-<?=$i?>" class="wrapper">
									<?php if($rand == 0): ?>
										<div class="input-group">
											<input type="radio" class="required" name="q-<?=$i?>" value="<?=$data['result'][0][$i-1]['id']?>"> <?=$data['result'][0][$i-1]['question']?>
										</div>
										<br/>
										<div class="input-group">
											<input type="radio" class="required" name="q-<?=$i?>" value="<?=$data['result'][1][$i-1]['id']?>"> <?=$data['result'][1][$i-1]['question']?>
										</div>
									<?php else: ?>
										<div class="input-group">
											<input type="radio" class="required" name="q-<?=$i?>" value="<?=$data['result'][1][$i-1]['id']?>"> <?=$data['result'][1][$i-1]['question']?>
										</div>
										<br/>
										<div class="input-group">
											<input type="radio" class="required" name="q-<?=$i?>" value="<?=$data['result'][0][$i-1]['id']?>"> <?=$data['result'][0][$i-1]['question']?>
										</div>
									<?php endif; ?>

									<?php if($i != $max_length){ ?>
										<div style="margin-top: 25px;text-align:center"> 
											<button type="button" class="btn btn-danger" onclick="nextPage(<?=$i?>);">Lanjut</button>
										</div>
									<?php }else{ ?> 
										<div id="container-show-result" style="margin-top: 25px;text-align:center"> 
											<input type="submit" class="btn btn-success" value="Lihat Hasil!">
										</div>
									<?php } ?>
								</div>
							<?php } ?>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- NOTES => PLEASE DISPLAY THE RESULT AFTER USER FILLED ALL QUESTIONER -->
<div id="result" style="display: none">
	<div class="container">
		<div class="row">
			<div id="content-result" class="col-lg-12">
				<!-- <div id="title">
					Inilah <span class="font-red">#CalonPresiden</span><br/>yang kamu banget!
				</div>
				<div id="logo">
					<img src="http://www.indonews.com/asset/assets/posts/medium/Capres-_Cawapres.jpg" width="200" />
				</div>
				<div id="candidate-name">
					<h1>PRABOWO-HATTA</h1>
					<h1>JOKOWI-JK</h1>
				</div>
				<div style="margin-top: 25px;text-align:center;border-top: solid 1px #ebebeb; padding: 20px"> 
					<p style="font-size: 18px">
						Klik link di bawah untuk melihat latar belakang dari <span class="font-red"><strong>#CalonPresiden</strong></span> 
					</p>
					<a href="..." class="btn btn-danger">Wiki!</a>
				</div> -->
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		/* Ajax to get data */
		// $.ajax({
		// 	url: "<?=base_url()?>data/promises/json",
		// 	dataType: "JSON"
		// }).done(function(result) {
		// 	console.log(result);
		// });
		/* submit form */
		// jQuery("#myform").validate();
		jQuery("#myform").submit(function() {
	    	// if(jQuery("#myform").valid()){
	    		$('#question-title').html('Mohon tunggu sesaat ...')
	    		$('#question-answer').append('<i class="fa fa-smile-o fa-spin fa-5x font-red"></i>');
	    		$('#question-answer').css({
	    			'text-align': 'center',
	    			'padding': 5 
	    		})
	    		$('#myform').hide();
		        jQuery.ajax({
		            type: "POST",
		            url: "<?=base_url()?>question/submit",
		            data: jQuery("#myform").serialize(),
		            dataType: "JSON"
		        }).done(
		            function (result) {
		            	if(result['data']['results']['count'] > 0){
		            		var data = result['data']['results']['caleg'];
		            		var html = '';
		            		html += "<div id='title'>"+
								"Inilah <span class='font-red'>#CalonPresiden</span><br/>yang kamu banget!"+
							"</div>"+
							"<div id='logo'>"+
								"<a href='"+base_url+"wiki'><img src='"+base_url+"assets/img/"+data[0]['id']+".jpg' width='200' /></a>"+
							"</div>"+
							"<div id='candidate-name'>"+
								"<h1>"+data[0]['nama']+" - "+data[1]['nama']+"</h1>"+
							"</div>"+
							"<div style='margin-top: 25px;text-align:center;border-top: solid 1px #ebebeb; padding: 20px'>"+
								"<p style='font-size: 18px'>"+
									"Klik link di bawah untuk melihat latar belakang dari <span class='font-red'><strong>#CalonPresiden</strong></span>"+
								"</p>"+
								"<a href='"+base_url+"wiki' class='btn btn-danger'>Wiki!</a>"+
							"</div>";
							$('#content-result').html(html);
			            	$('#content').hide();
			            	$('#result').show();
			            }else{
			            	var html = '<div>Pilihlah jawaban anda untuk temukan Presiden dan Wakil Presiden yang kamu banget!</div>'+
			            	'<br><a href="<?=base_url()?>question" class="btn btn-danger" style="font-size:20px;">Coba lagi</a>';
			            	$('#question-title').html(html);
			            	$('#question-answer').hide();
			            }
		        	}
		        );
		    // }
		    return false;
		});
	});
</script>