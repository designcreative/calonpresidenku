<!DOCTYPE html>
<html lang="en">
<head>
	<script type="text/javascript">var base_url = '<?=base_url()?>';</script>
	<meta charset="utf-8">
	<title><?=$title?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/designcreative.style.css">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
</head>
<body>
	<div id="top-page">
		<div id="header">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="pad-10 font-white pull-left">
							<a href="<?=base_url()?>" style="color:#fff;text-decoration:none"><strong>#CalonPresidenkuDanWakilPresidenku</strong></a>
						</div>
						<!-- Go to your Addthis.com Dashboard to update any options -->
						<div class="pad-10 font-white pull-right" id="group-share">
							<div class="addthis_sharing_toolbox"></div>
						</div>
						<!-- <div class="pad-10 font-white pull-right">
							<a href="<?=base_url()?>wiki" style="color:#fff;text-decoration:none"><strong>Wiki</strong></a>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</div>