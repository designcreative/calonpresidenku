	<div id="footer">
		<div class="inner">
			Copyright &copy; 2014. <a href="http://designcreative.in">DesignCreative</a>. Be Inspired!
		</div>
	</div>
	
	<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.validate.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-51872357-1', 'calonpresidenku.com');
		ga('send', 'pageview');
		ga('set', '&uid', 'UA-51872357-1'); // Tetapkan ID pengguna menggunakan user_id yang telah masuk ke sistem.
	</script>
	<!-- AddThis Pro BEGIN -->
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5349941e295c591e"></script>
	<!-- AddThis Pro END -->
</body>
</html>