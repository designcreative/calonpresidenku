<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Question extends MY_Controller {

	function __construct() {
        parent::__construct();
    }

    public function index(){
        $this->template['data'] = $this->promises();

        $this->template['title'] = '#CalonPresidenKu - Questioner';
        $this->render_page('question');
    }

    public function submit_question(){
        $jw = 0;
        $ps = 0;
        for ($i=1; $i <= 10; $i++) { 
            if(isset($_POST['q-'.$i]) && $_POST['q-'.$i] != ''){
                $data = $this->get_promises_by_id($_POST['q-'.$i]);
                
                if($data['data']['results']['count'] > 0){

                    foreach($this->db->list_fields('promises') as $row) {
                        if (isset($data['data']['results']['promise'][0][$row]) && !is_array($data['data']['results']['promise'][0][$row])) $field[$row] = $this->cek_null(trim($data['data']['results']['promise'][0][$row]));
                    }

                    $field['created_date'] = date("Y-m-d H:i:s");

                    $this->db->insert('promises', $field);

                    if($data['data']['results']['promise'][0]['id_calon'] == 'jw'){
                        $jw++;
                    }
                    else{
                        $ps++;
                    }
                }
            }
        }
        unset($field);
        if($jw > $ps){
            $calon = $this->get_calon_presiden('', 'jw');
            $cawapres = $this->get_calon_presiden('', 'jk');

            $calon['data']['results']['caleg'][1] = $cawapres['data']['results']['caleg'][0];

            foreach($this->db->list_fields('result_capres') as $row) {
                if (isset($calon['data']['results']['caleg'][0][$row]) && !is_array($calon['data']['results']['caleg'][0][$row])) $field[$row] = $this->cek_null(trim($calon['data']['results']['caleg'][0][$row]));
            }

            $field['created_date'] = date("Y-m-d H:i:s");

            $this->db->insert('result_capres', $field);
            
            echo json_encode($calon);
        }else if($jw < $ps){
            $calon = $this->get_calon_presiden('', 'ps');
            $cawapres = $this->get_calon_presiden('', 'hr');
            
            $calon['data']['results']['caleg'][1] = $cawapres['data']['results']['caleg'][0];

            foreach($this->db->list_fields('result_capres') as $row) {
                if (isset($calon['data']['results']['caleg'][0][$row]) && !is_array($calon['data']['results']['caleg'][0][$row])) $field[$row] = $this->cek_null(trim($calon['data']['results']['caleg'][0][$row]));
            }

            $field['created_date'] = date("Y-m-d H:i:s");

            $this->db->insert('result_capres', $field);

            echo json_encode($calon);
        }else{
            echo json_encode(array('data' => array('results' => array('count' => 0))));
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */