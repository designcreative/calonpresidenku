<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {

	function __construct() {
        parent::__construct();
    }

    public function index(){
        //$data = $this->promises();

        $this->template['title'] = '#CalonPresidenKu';
        //$this->template['data'] = $data;

        $this->render_page('index');
    }

    public function profile(){
        //$this->template['data'] = get_calon_presiden();
        $this->template['title'] = '#CalonPresidenKu - Wiki';
        $this->render_page('profile');
    }

    public function profile_detail(){
        if(isset($_GET['calon']) && $_GET['calon'] != ''){
            $calon = explode(",", $_GET['calon']);
            $count = 0;

            foreach($calon as $row){
                $temp = $this->get_calon_presiden('', $row);
                $this->template['data'][$count] = $temp['data']['results']['caleg'][0];
                $count++;
            }

            $this->template['title'] = '#CalonPresidenKu - Wiki';
            $this->render_page('profile_detail');
        }else{
            redirect('/wiki/', 'refresh');
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */