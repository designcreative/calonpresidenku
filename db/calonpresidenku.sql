-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Waktu pembuatan: 10. Juni 2014 jam 21:02
-- Versi Server: 5.5.9
-- Versi PHP: 5.3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `calonpresidenku`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `promises`
--

CREATE TABLE `promises` (
  `id_promises` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(50) NOT NULL,
  `id_calon` varchar(50) NOT NULL,
  `context_janji` text NOT NULL,
  `created_date` varchar(50) NOT NULL,
  PRIMARY KEY (`id_promises`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data untuk tabel `promises`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `result_capres`
--

CREATE TABLE `result_capres` (
  `id_result_capres` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(50) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `role` varchar(50) NOT NULL,
  `id_running_mate` varchar(50) NOT NULL,
  `created_date` varchar(50) NOT NULL,
  PRIMARY KEY (`id_result_capres`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data untuk tabel `result_capres`
--

