<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_global extends CI_Model {
    
    function __construct()
    {
        parent::__construct();
    }
    
    public function get_data($array)
    {
        if (isset($array['table'])) {
            if (isset($array['select']))    $this->db->select($array['select']);
            if (isset($array['join']))      $this->db->join($array['join'][0],$array['join'][1]);
            if (isset($array['join2']))     $this->db->join($array['join2'][0],$array['join2'][1]);
            if (isset($array['where']))     $this->db->where($array['where']);
            if (isset($array['like']))      $this->db->like($array['like']);
            if (isset($array['or_like']))   $this->db->or_like($array['or_like']);
            if (isset($array['not_like']))  $this->db->not_like($array['not_like']);
            if (isset($array['order_by']))  $this->db->order_by($array['order_by']);
            if (isset($array['group_by']))  $this->db->group_by($array['group_by']);
            if (isset($array['limit']))     $this->db->limit($array['limit']);
            $this->db->from($array['table']);
            if (isset($array['data']) && $array['data'] == 'row') 
                return $this->db->get()->row_array();
            else
                return $this->db->get()->result_array();
        } else
            return array();
    }
}